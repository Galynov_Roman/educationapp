import React from 'react';
import * as ReactNavigation from 'react-navigation';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AppNavigation from './AppNavigation';

// here is our redux-aware smart component
function ReduxNavigation({ dispatch, nav }) {
  const navigation = ReactNavigation.addNavigationHelpers({
    dispatch,
    state: nav,
  });

  return <AppNavigation navigation={navigation} />;
}

ReduxNavigation.propTypes = {
  dispatch: PropTypes.any.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({ nav: state.nav });
export default connect(mapStateToProps)(ReduxNavigation);
