import React, { Component } from 'react';
import { Text, View } from 'react-native';

// Styles
import styles from './Styles/HomeScreenStyles';

export default class HomeScreen extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <Text style={styles.sectionText}>Home Screen</Text>
      </View>
    );
  }
}
